package Clases;

public class Jugador 
{
    private String nombre;
    private int votos;
    
    public Jugador(String nombre, int votos)
    {
        this.nombre = nombre;
        this.votos = votos;
    }
    
    public void setVotos(int votos)
    {
        this.votos = votos;
    }
    
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    
    public String getNombre()
    {
        return this.nombre;
    }
    
    public int getVotos()
    {
        return this.votos;
    }
}