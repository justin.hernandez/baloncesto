<%@page import="Clases.Jugador" %>
<%@page import="java.util.ArrayList" %>
<%@page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Votos actuales</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>

    <%
        ArrayList<Jugador> jugadores = (ArrayList<Jugador>) request.getSession().getAttribute("jugadores"); 
    %>

    <body class="votos">
        <h1> Tabla de votos actuales </h1>

        <hr>

        <table id="tabla" summary="Tabla de votos por jugador" border="1">
            <tr>
                <th>Nombre</th>
                <th>Votos</th>
            </tr>
            <%                 
                for (Jugador j : jugadores)
                {            
            %>
                <tr>
                    <td> <%= j.getNombre() %> </td>
                    <td> <%= j.getVotos() %> </td>
                </tr>
            <% 
                }
            %>
        </table>
        <hr>
        <br>
        <a href="index.html"> Ir al comienzo</a>
    </body>

</html>