import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebElement;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;
import org.openqa.selenium.By;

public class PruebasPhantomjsIT 
{
    private static WebDriver driver=null;
   
    @Test
    public void tituloIndexTest()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);

        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");

        System.out.println(driver.getTitle());

        driver.close();
        driver.quit();
    }

    @Test
    public void pfa()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);

        driver.navigate().to("http://localhost:8080/Baloncesto/");

        // Click boton poner votos a cero
        driver.findElement(By.id("resetearvotos")).click(); 

        // Click ver pagina votos
        driver.findElement(By.id("vervotos")).click();

        // elemento tabla
        WebElement tabla = driver.findElement(By.id("tabla"));

        // filas de la tabla
        List<WebElement> filas = tabla.findElements(By.tagName("tr"));

        // por cada una de las filas
        for(int i=1; i<filas.size(); i++) {

            // lista de columnas
            List<WebElement> columnas = filas.get(i).findElements(By.tagName("td"));

            // comprueba que la columna 1, que 
            // corresponde a los votos, tiene texto "0"
            assertEquals("0", columnas.get(1).getText());
        }

        driver.close();
        driver.quit();
    }

    @Test
    public void pfb()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);

        driver.navigate().to("http://localhost:8080/Baloncesto/");

        String nombreJugador = "Kobe";

        // selecciona el radio button "Otros"
        driver.findElement(By.id("otros")).click();

        // inserta en el campo de texto el nombre del nuevo jugador
        driver.findElement(By.id("txtOtros")).sendKeys(nombreJugador);

        //votar, nos lleva a TablaVotos.jsp
        driver.findElement(By.id("votar")).click();

        // volvemos a index.html
        driver.findElement(By.id("volver")).click();

        // click ver pagina votos
        driver.findElement(By.id("vervotos")).click();

        // elemento tabla y filas de la tabla
        WebElement tabla = driver.findElement(By.id("tabla"));
        List<WebElement> filas = tabla.findElements(By.tagName("tr"));

        // por cada una de las filas
        for(int i=1; i<filas.size(); i++) {

            // lista de columnas
            List<WebElement> columnas = filas.get(i).findElements(By.tagName("td"));

            // si corresponde a la fila del nuevo jugador insertado
            if(columnas.get(0).getText().equals(nombreJugador))
            {
                // comprueba que tiene un voto
                assertEquals("1", columnas.get(1).getText());
            }
        }

        driver.close();
        driver.quit();
    }
}